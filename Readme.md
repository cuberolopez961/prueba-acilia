# Prueba acilia

# Uso del contenedor docker. 

Para ejecutar la aplicación podemos hacer uso del contenedor de docker que viene en el repositorio. Para ello ejecutamos
~~~~
docker-compose up -d  --build
~~~~
Esto desplegar un contenedor en el que se ejecutar nuestra aplicación.
# Despliegue de BBDD
## Mediante doctrine fixture
Una vez tenemos la aplicacion desplegada podemos ejecutar el siguiente comando de symfony para crear el schema de la BBDD:
~~~~
php bin/console doctrine:database:create
~~~~
Una vez creado el schema procedemos a ejecutar las migraciones:
~~~~
php bin/console doctrine:migrations:migrate
~~~~
Y, para generar datos de prueba, podemos cargar los fixtures:
~~~~
php bin/console doctrine:fixtures:loads
~~~~
<b>Nota importante:</b> los fixtures no añaden datos nuevos al schema. Purga el schema y carga los datos. 
## Mediante dumps
Tambien el repositorio contiene una serie de consultas SQL para crear las tablas y cargar datos de prueba. 
# Documentación de la Api. 
Accediendo a la siguiente ruta de la aplicacion:

~~~~
/api/doc
~~~~

Podrá obtener toda la información sobre los requisitos de los endpoints.