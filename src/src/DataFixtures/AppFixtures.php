<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $milkCategory = new Category();
        $milkCategory->setName("Productos lacteos");
        $milkCategory->setDescription("Productos derivados de la leche");

        $cleaningCategory = new Category();
        $cleaningCategory->setName("Productos de limpieza");
        $cleaningCategory->setDescription("Productos de limpieza de hogar");

        $electronicCateogry = new Category();
        $electronicCateogry->setName("Electronica e informatica");
        $electronicCateogry->setDescription("Productos electronicos , y accesorios");

        $manager->persist($milkCategory);
        $manager->persist($electronicCateogry);
        $manager->persist($cleaningCategory);
        $manager->flush();

        $milkProduct = new Product();
        $milkProduct->setName("Leche");
        $milkProduct->setCategory($milkCategory);
        $milkProduct->setCurrency("USD");
        $milkProduct->setPrice(10);

        $bleachProduct = new Product();
        $bleachProduct->setName("Lejia");
        $bleachProduct->setCategory($cleaningCategory);
        $bleachProduct->setPrice(1.25);
        $bleachProduct->setCurrency("USD");
        $bleachProduct->setFeatured(true);

        $headphonesProduct = new Product();
        $headphonesProduct->setName("HeadPhones");
        $headphonesProduct->setCategory($electronicCateogry);
        $headphonesProduct->setCurrency("EUR");
        $headphonesProduct->setPrice(20);

        $manager->persist($milkProduct);
        $manager->persist($bleachProduct);
        $manager->persist($headphonesProduct);

        $manager->flush();
    }
}
