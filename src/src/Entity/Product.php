<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product implements IEntity
{
    const ALLOWED_CURRENCIES = ["EUR","USD"];
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"Featured","Product"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Groups({"Featured","Product"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="Products")
     * @Groups({"Product","Featured"})
     */
    private $category;

    /**
     * @ORM\Column(type="float", length=255)
     * @Assert\NotBlank(message="{{ value }} should not be blank ")
     * @Assert\Regex(pattern="/\d+(\.\d+)?/i", message="{{ value }} is not numeric")
     * @Groups({"Featured","Product"})
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"Featured","Product"})
     */
    private $featured = false;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Choice(Product::ALLOWED_CURRENCIES)
     * @Groups({"Featured", "Product"})
     */
    private $currency;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getFeatured(): ?bool
    {
        return $this->featured;
    }

    public function setFeatured(bool $featured): self
    {
        $this->featured = $featured;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
