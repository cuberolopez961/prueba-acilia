<?php
namespace App\Services\Validator;

use App\Services\Serializer\SerializerService;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidatorService
{

    private ValidatorInterface $_validator;

    private array $_errors;
    private SerializerInterface $_serializer;

    public function __construct(ValidatorInterface $validator, SerializerInterface $serializerService)
    {
        $this->_validator = $validator;
        $this->_errors = [];
        $this->_serializer = new $serializerService;
    }

    public function validate($object): bool {
        $errors =  $this->_validator->validate($object);
        foreach($errors as $error){
            array_push($this->_errors, $error->getMessage());
        }
        return count($this->_errors) <= 0;
    }

    public function getErrors() {
        return $this->_errors;
    }

}