<?php


namespace App\Services\Validator;


use Throwable;

class ValidationException extends \Exception
{
    private array $_errors;
    public function __construct($errors=[],$message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->_errors = $errors;
    }

    public function getErrors(){
        return $this->_errors;
    }

}