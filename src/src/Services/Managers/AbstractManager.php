<?php

namespace App\Services\Managers;

use App\Entity\IEntity;
use App\Services\Validator\ValidationException;
use App\Services\Validator\ValidatorService;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $_em;
    /**
     * @var ValidatorService
     */
    protected $_validator;

    public function __construct(EntityManagerInterface $manager, ValidatorService $validatorService)
    {
        $this->_em = $manager;
        $this->_validator = $validatorService;
    }

    public function remove(IEntity $entity)
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }

    /**
     * @param IEntity $entity
     * @throws ValidationException
     */
    public function save(IEntity $entity)
    {
        if(!$this->_validator->validate($entity)){
            throw new ValidationException($this->_validator->getErrors());
        }
        $this->_em->persist($entity);
        $this->_em->flush();
    }

    public abstract function find(int $id);
    public abstract function all();
}