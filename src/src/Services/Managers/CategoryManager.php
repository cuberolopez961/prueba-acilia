<?php


namespace App\Services\Managers;


use App\Entity\Category;
use App\Entity\IEntity;
use App\Repository\CategoryRepository;
use App\Services\Validator\ValidationException;
use App\Services\Validator\ValidatorService;
use Doctrine\ORM\EntityManagerInterface;

class CategoryManager extends AbstractManager
{
    public function __construct(EntityManagerInterface $manager, ValidatorService $validatorService)
    {
        parent::__construct($manager, $validatorService);
    }

    /**
     * @param string|null $name
     * @param string|null $description
     * @throws ValidationException
     */
    public function add(?string $name, ?string $description)
    {

        $category = new Category();
        $category->setName($name);
        $category->setDescription($description);

        if (!$this->_validator->validate($category)) {
            throw new ValidationException($this->_validator->getErrors());
        }

        $this->save($category);


    }

    /**
     * @param int|null $id
     * @param string|null $name
     * @param string|null $description
     * @throws ValidationException
     * @throws \Exception
     */
    public function update(?int $id, ?string $name, ?string $description){
        $category = $this->find($id);
        if(!$category){
            throw new \Exception("category not found");
        }
        $category->setName($name);
        $category->setDescription($description);

        if(!$this->_validator->validate($category)){
            throw new ValidationException($this->_validator->getErrors());
        }
        $this->save($category);
    }

    /**
     * @param IEntity $entity
     */
    public function remove(IEntity $entity){
        /**
         * @var Category $category
         */
        $category = $entity;


        $products = $category->getProducts();
        foreach ($products as $product){
            $product->setCategory(null);
            $this->save($product);
        }
        parent::remove($category);
    }

    public function find(int $id): Category
    {
        /**
         * @var CategoryRepository $repo
         */
        $repo = $this->_em->getRepository(Category::class);
        return $repo->find($id);
    }

    public function all()
    {
        /**
         * @var CategoryRepository $repo
         */
        $repo = $this->_em->getRepository(Category::class);
        return $repo->findAll();
    }
}