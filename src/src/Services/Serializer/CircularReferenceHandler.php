<?php


namespace App\Services\Serializer;


class CircularReferenceHandler
{

    public function __invoke($object)
    {
        return strval($object);
    }

}