<?php


namespace App\Services\RatesApi;


use App\Services\Serializer\SerializerService;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiService
{
    /**
     * @var HttpClientInterface
     */
    private $_httpClient;
    /**
     * @var SerializerInterface
     */
    private $_serializer;

    public function __construct(HttpClientInterface $httpClient, SerializerInterface $serializerService)
    {
        $this->_httpClient = $httpClient;
        $this->_serializer = $serializerService;
    }

    /**
     * @param string $amount
     * @param string $fromCurrency
     * @param string $toCurrency
     * @return float|int
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function conversion(string $amount, string $fromCurrency, string $toCurrency){
        if($fromCurrency === $toCurrency){
            return $amount;
        }
        $response = $this->_httpClient->request("GET", "https://api.exchangeratesapi.io/latest",[
            "query"=>[
                "base"=>$fromCurrency,
                "symbols"=>$toCurrency
            ]
        ]);
        /**
         * @var RatesResponse $rate
         */
        $rate = $this->_serializer->deserialize($response->getContent(), RatesResponse::class, 'json');



        $rate = $rate->getRates()[$toCurrency];

        $convertedAmount = doubleval($amount) * doubleval($rate);
        return round($convertedAmount, 2);



    }
}