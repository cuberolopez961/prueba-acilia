<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Services\Managers\ProductManager;
use App\Services\RatesApi\ApiService;
use App\Services\Validator\ValidationException;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Swagger\Annotations as SWG;

class ProductController extends AbstractController
{
    /**
     * @var CategoryRepository $_categoryRepo
     */
    private $_categoryRepo;
    /**
     * @var SerializerInterface
     */
    private $_serializer;
    /**
     * @var EntityManagerInterface
     */
    private $_em;
    /**
     * @var ApiService
     */
    private $_apiService;
    /**
     * @var ProductManager
     */
    private $_productManager;

    public function __construct(EntityManagerInterface $em, ProductManager $manager, SerializerInterface $serializer, ApiService $apiService)
    {
        $this->_serializer = $serializer;
        $this->_em = $em;
        $this->_productManager = $manager;
        $this->_categoryRepo = $this->_em->getRepository(Category::class);
        $this->_apiService = $apiService;
    }

    /**
     * @Route("/api/v1/products", name="addProduct", methods={"POST"})
     * @SWG\Post(
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(name="category",
     *     in="body",
     *     description="product that will be added",
     *     @SWG\Schema(
     *          @SWG\Property(property="name", description="name of category" ,type="string", example="Lacteos"),
     *          @SWG\Property(property="price", type="float", example=10.0),
     *          @SWG\Property(property="category", type="object", example={"id": 1}),
     *          @SWG\Property(property="currency", type="string", example="USD"),
     *          @SWG\Property(property="featured", type="boolean", example=false)
     *      )
     *     ),
     *
     *     @SWG\Response(
     *          response="200",
     *          description="product added"
     *      )
     * )
     * @param Request $request
     * @return Response
     */
    public function add(Request $request)
    {
        $category = null;
        $content = $request->getContent();
        $content = $content ? $content : '{}';

        try {
            /**
             * @var Product $product
             */
            $product = $this->_serializer->deserialize($content, Product::class, 'json');
            $this->_productManager->save($product);
        } catch (NotEncodableValueException $e) {
            return new Response($this->prepareMessage("Bad Request Format"), Response::HTTP_BAD_REQUEST);
        } catch (ValidationException $e) {
            $json = $this->_serializer->serialize($e->getErrors(), 'json');
            return new Response($json,Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e){
            return new Response($this->prepareMessage("Internal Server Error"), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new Response($this->_serializer->serialize($product, 'json'), Response::HTTP_OK);

    }

    /**
     * @Route("/api/v1/products/featured", name="featuredProducts", methods={"GET"})
     * @SWG\Get(produces={"application/json"},
     *     @SWG\Parameter(name="currency", in="query", type="string", description="Currency to conversion (USD or EUR)"),
     *
     *     @SWG\Response(response="200", description="all featured products")
     *    )
     * @param Request $request
     * @return Response
     */
    public function featured(Request $request)
    {
        $currency = $request->query->get('currency', null);
        if ($currency !== null && !in_array($currency, Product::ALLOWED_CURRENCIES)) {
            return new Response("Currency value not allowed", Response::HTTP_BAD_REQUEST);
        }
        $products = $this->_productManager->getFeatures();
        if (null !== $currency) {
            $products = array_map(function (Product $product) use ($currency) {
                $price = $product->getPrice();
                $price = $this->_apiService->conversion($price, $product->getCurrency(), $currency);
                $product->setPrice(strval($price));
                $product->setCurrency($currency);
                return $product;
            }, $products);
        }


        return new Response($this->_serializer->serialize($products, 'json', ['groups' => ['Featured']]), Response::HTTP_OK);
    }

    /**
     * @Route("/api/v1/products", name="getProducts", methods={"GET"})
     * @SWG\Get(
     *     @SWG\Response(response="200", description="all product")
     * )
     */
    public function all()
    {
        $products = $this->_productManager->all();

        return new Response($this->_serializer->serialize($products, 'json'), Response::HTTP_OK);
    }

    /**
     * @Route("/api/v1/products/{id}", name="updateProduct", methods={"PUT"})
     * @SWG\Put(
     *     @SWG\Parameter(name="id", in="path", type="integer", description="product's id"),
     *     @SWG\Parameter(name="product",
     *     in="body",
     *     description="product that will be updated",
     *     @SWG\Schema(
     *      @SWG\Property(property="name", type="string", example="leche"),
     *     @SWG\Property(property="price", type="float", example=10.0),
     *     @SWG\Property(property="currency", type="string", example="USD"),
     *     @SWG\Property(property="category", type="object", example={"id": 3}),
     *     @SWG\Property(property="featured", type="boolean", example=false)
     *     )
     *     ),
     *     @SWG\Response(response="200", description="product updated")
     *
     * )
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function update(Request $request, $id = 0)
    {
        $product = $this->_productManager->find($id);
        if (null === $product) {
            return new Response("product not found", Response::HTTP_BAD_REQUEST);
        }
        $content = $request->getContent();
        $content = $content ? $content: '{}';

        try {
            /**
             * @var Product $product
             */
            $product = $this->_serializer->deserialize($content,
                Product::class,
                'json',
                [AbstractNormalizer::OBJECT_TO_POPULATE => $product]);
            $this->_productManager->save($product);
        } catch (NotEncodableValueException $e) {
            return new Response($this->prepareMessage("Bad Request Format"), Response::HTTP_BAD_REQUEST);
        } catch (ValidationException $e) {
            return new Response($this->_serializer->serialize($e->getErrors(), 'json'), Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }

        return new Response($this->_serializer->serialize($product, 'json'), Response::HTTP_OK);

    }

    /**
     * @Route("/api/v1/products/{id}", name="deleteProduct", methods={"DELETE"})
     * @SWG\Delete(
     *     @SWG\Parameter(name="id", in="path", type="integer", description="product's id"),
     *     @SWG\Response(response="200", description="product that has been deleted")
     * )
     * @param int $id
     * @return Response
     */
    public function delete($id = 0)
    {
        $product = $this->_productManager->find($id);
        if (null === $product) {
            return new Response("product not found", Response::HTTP_BAD_REQUEST);
        }
        $this->_productManager->remove($product);

        return new Response($this->_serializer->serialize($product, 'json'), Response::HTTP_OK);
    }

    /**
     * @param string $message
     * @return string
     */
    public function prepareMessage(string $message): string
    {
        $error = $this->_serializer->serialize(["Error"=>$message], 'json');
        return $error;
    }
}
