<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Services\Managers\CategoryManager;
use App\Services\Serializer\SerializerService;
use App\Services\Validator\ValidationException;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class CategoryController
 * @package App\Controller
 */
class CategoryController extends AbstractController
{


    /**
     * @var SerializerInterface
     */
    private $_serializer;
    /**
     * @var CategoryManager
     */
    private $_manager;

    public function __construct(CategoryManager $manager, SerializerInterface $serializer)
    {
        $this->_serializer = $serializer;
        $this->_manager = $manager;
    }

    /**
     * @Route("/api/v1/categories", name="addCategory", methods={"POST"})
     * @SWG\Post(
     *     @SWG\Parameter(name="category",
     *     in="body",
     *     description="category that will be added",
     *     @SWG\Schema(
     *          @SWG\Property(property="name", type="string", example="Productos Lacteos"),
     *          @SWG\Property(property="description", type="string", example="Productos provenientes de la leche")
     *      )
     *     ),
     *     @SWG\Response(response="200", description="category that has been added")
     * )
     * @param Request $request
     * @return Response
     */
    public function add(Request $request)
    {
        try {
            $content = $request->getContent();
            $content = $content ? $content : '{}';

            /**
             * @var Category $category
             */
            $category = $this->_serializer->deserialize($content,Category::class, 'json');
            $this->_manager->save($category);
        } catch (NotNormalizableValueException $e) {
            return new Response($this->prepareMessage("Bad Request Format"),Response::HTTP_BAD_REQUEST);
        } catch (ValidationException $e) {
            return new Response($this->_serializer->serialize($e->getErrors(), 'json'),Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e){
            return new Response($this->prepareMessage("Internal Server Error"), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new Response($this->_serializer->serialize($category, 'json'), Response::HTTP_OK);


    }

    /**
     * @Route("/api/v1/categories", name="getCategories", methods={"GET"})
     * @SWG\Get(
     *     @SWG\Response(response="200", description="all categories")
     * )
     */
    public function all()
    {
        /**
         * @var CategoryRepository $repository
         */
        $categories = $this->_serializer->serialize($this->_manager->all(), 'json');
        return new Response($categories, Response::HTTP_OK);
    }

    /**
     * @Route("/api/v1/categories/{id}", name="modifyCategories", methods={"PUT"})
     * @SWG\Put(
     *     @SWG\Parameter(name="id", in="path", type="integer", description="category's id"),
     *     @SWG\Parameter(name="category", in="body",
     *     @SWG\Schema(
     *          @SWG\Property(property="name", type="string", example="Limpieza"),
     *          @SWG\Property(property="description", type="string", example="Productos de limpieza de hogar")
     *      )
     * ),
     *     @SWG\Response(response="200", description="category that has been updated")
     * )
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function update(Request $request, $id = 0)
    {
        try {
            $category = $this->_manager->find($id);
            if(!$category){
                return new Response("category not found", Response::HTTP_NOT_FOUND);
            }
            $content = $request->getContent();
            $content = $content ? $content : '{}';
            /**
             * @var Category $category
             */
            $category = $this->_serializer->deserialize($content,
                Category::class,
                'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $category]);
            $this->_manager->save($category);

        } catch (NotEncodableValueException $e) {
            return new Response($this->prepareMessage("Bad request format"), Response::HTTP_BAD_REQUEST);
        } catch (ValidationException $e) {
            return new Response($this->_serializer->serialize($e->getErrors(),'json'),Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return new Response($this->prepareMessage("Internal Server Error"), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new Response($this->_serializer->serialize($category, 'json'), Response::HTTP_OK);

    }

    /**
     * @Route("/api/v1/categories/{id}", name="deleteCategories", methods={"DELETE"})
     * @SWG\Delete(
     *     @SWG\Parameter(name="id", in="path",type="integer", description="category's id"),
     *     @SWG\Response(response="200", description="category that has been deleted")
     * )
     * @param int $id
     * @return Response
     */
    public function delete($id = 0)
    {
        $category = $this->_manager->find($id);
        if (null === $category) {
            return new Response("category not found", Response::HTTP_BAD_REQUEST);
        }
            $this->_manager->remove($category);

        return new Response($this->_serializer->serialize($category, 'json'), Response::HTTP_OK);
    }

    /**
     * @param string $message
     * @return string
     */
    public function prepareMessage(string $message): string
    {
        $error = $this->_serializer->serialize(["Error" => $message], 'json');
        return $error;
    }

}
